from flask import request, jsonify
from config import datastore
from . import system_module_1
import pandas as pd
import threading


def process_file(data_frame):
    delete_process_file()
    list_upsert = []
    data_store_client = datastore.Client(project='testmichaelpage')
    for count, row in data_frame.iterrows():
        entity = datastore.Entity(key=data_store_client.key('task'))
        entity.update(dict(row))
        list_upsert.append(entity)
    data_store_client.put_multi(list_upsert)


def delete_process_file():
    data_store_client = datastore.Client(project='testmichaelpage')
    query = data_store_client.query(kind='task')
    list_data = list(query.fetch())
    data_store_client.delete_multi(list_data)


@system_module_1.route("/load_data/", methods=('post', ))
def load_data():
    file_csv = request.files['file_csv']
    data_frame = pd.read_csv(file_csv)
    threading.Thread(target=process_file, args=(data_frame,)).start()
    return jsonify({"response": "file processed"})


@system_module_1.route("/delete_data/", methods=('delete', ))
def delete_data():
    threading.Thread(target=delete_process_file).start()
    return jsonify({"response": "Truncate table"})
