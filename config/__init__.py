from google.cloud import datastore
from flask import Flask


def create_app():
    from system_module_1 import system_module_1
    from system_module_2 import system_module_2
    app = Flask(__name__)
    app.register_blueprint(system_module_1)
    app.register_blueprint(system_module_2)
    return app


def get_storage_query():
    data_store_client = datastore.Client(project='testmichaelpage')
    query = data_store_client.query(kind='task')
    return query
