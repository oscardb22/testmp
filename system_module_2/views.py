from flask import request, jsonify
from config import get_storage_query
from . import system_module_2


@system_module_2.route("/find_data/")
def find_data():
    data_dict = request.args.to_dict()
    query = get_storage_query()
    for key, data in data_dict.items():
        query.add_filter(key, "=", data)
    results = list(query.fetch())
    return jsonify(results)

