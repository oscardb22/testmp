#!/usr/bin/python3
import click
import requests


@click.command()
@click.argument('find')
@click.argument('value')
def cli(find, value):
    response = requests.get(
        'https://testmichaelpage.ue.r.appspot.com/system_module_2/find_data/', params={find: value}
    )
    json_response = response.json()
    if len(json_response) == 0:
        print(f'Cannot fund something related with field [{find}] and value [{value}]')
    else:
        print(f"{response.json()[0].get('description')}")


if __name__ == '__main__':
    cli()
